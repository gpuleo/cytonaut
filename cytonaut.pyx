# Cytonaut - Python interface to Nauty
# Written in 2015 by Gregory J. Puleo
# Last updated in 2021 by ", gjp0007@auburn.edu

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool as cbool

import networkx

cdef extern from "nygraph.hpp":
  cdef cppclass NyGraph:
    NyGraph(int)
    void add_edge(int, int)
    void do_nauty(cbool)
    string canon_string()
    vector[int] canon_labeling
    vector[int] orbits    
    vector[int] ptn
    vector[vector[int]] automorphisms
    void get_automorphisms()
    
cdef class CytoGraph:
  cdef NyGraph *thisptr
  cdef public object G
  cdef public object translation
  cdef public object reverse_translation
  cdef int n
  def __cinit__(self, G=None, translation=None, color_attribute="color"):
    #G: a networkx graph
    if G is None:
      raise "need to pass G to CytoGraph"
    self.G = G
    if translation is None:
      #H = networkx.convert_node_labels_to_integers(G, discard_old_labels=False)
      H = networkx.convert_node_labels_to_integers(G, label_attribute="old_label")
      self.translation = {} #H.graph["node_labels"]
      for v in H:
        self.translation[H.nodes[v]["old_label"]] = v
    else:
      self.translation = translation
    self.reverse_translation = {}
    for v in self.G:
      self.reverse_translation[self.translation[v]] = v
    self.n = G.number_of_nodes()
    n = self.n
    self.thisptr = new NyGraph(n)
    for e in G.edges():
      self.thisptr.add_edge(self.translation[e[0]], self.translation[e[1]])
    # write ptn and lab using color data
    namelab = list(G.nodes())
    namelab.sort(key = lambda x : G.nodes[x].get(color_attribute, 0))
    # write ptn
    ptn = [1] * n
    for i in range(n-1):
      if G.nodes[namelab[i]].get(color_attribute, 0) != G.nodes[namelab[i+1]].get(color_attribute, 0):
        ptn[i] = 0
    ptn[n-1] = 0
    for i in range(n):
      self.thisptr.canon_labeling[i] = self.translation[namelab[i]]
      self.thisptr.ptn[i] = ptn[i]
      

  def __dealloc__(self):
    del self.thisptr

  def do_nauty(self, aut=False):
    self.thisptr.do_nauty(<cbool>aut)
    if aut:
      self.thisptr.get_automorphisms()

  def automorphisms(self):
    cdef int i
    cdef int j
    cdef int k
    ret = []
    for i in range(self.thisptr.automorphisms.size()):
      next = {}
      for v in self.G:
        j = self.translation[v]
        k = self.thisptr.automorphisms[i][j]
        next[v] = self.reverse_translation[k]
        #pass
      ret.append(next)
    return ret

  def canon_string(self):
    ret = []
    cdef string canon = self.thisptr.canon_string()
    cdef int i
    for i in range(canon.length() - 1): #subtract 1 to suppress the newline at the end
      ret.append(chr(canon.at(i)))
    return "".join(ret)

  def canon_translation(self):
    # translate node labels in G to the corresponding vertices
    #  of the canonical labeling
    ret = {}
    py_canon_labeling = [0]*self.n
    for i in range(self.n):
      py_canon_labeling[i] = self.thisptr.canon_labeling[i]
    for v in self.G:
      ret[v] = py_canon_labeling.index(self.translation[v])
    return ret
  
  def orbits(self):
    # return a dict of lists, each list corresponding to a full orbit
    ret = {}
    for v in self.G:
      v_orbit = self.thisptr.orbits[self.translation[v]]
      if v_orbit not in ret:
        ret[v_orbit] = [v]
      else:
        ret[v_orbit].append(v)
    return ret
    
