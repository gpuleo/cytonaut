all:
	make ext
cython:
	touch pyint.cpp
	rm pyint.cpp
	python3 cytup.py build_ext --inplace
ext:
	python3 setup.py build_ext --inplace
clean:
	touch build
	touch cytonaut.so
	rm -rf build
	rm cytonaut.so
install:
	python3 setup.py install
