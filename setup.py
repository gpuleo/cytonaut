from distutils.core import setup
from distutils.extension import Extension

ext = Extension("cytonaut", ["cytonaut.cpp"],
                extra_objects=["./nauty/nauty1.o", "./nauty/gtools.o", "./nauty/naututil1.o", "./nauty/nausparse.o", "./nauty/nautil1.o",
                               "./nauty/naugraph1.o", "./nauty/schreier.o", "./nauty/naurng.o", "./nauty/naugroup.o"],
                language="c++",
                extra_compile_args=["-O3"],
                include_dirs=["./nauty"])

setup(ext_modules = [ext])
