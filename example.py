#!/usr/bin/python

# Cytonaut - Python interface to Nauty
# Written in 2015 by Gregory J. Puleo
# Last updated in 2021 by ", gjp0007@auburn.edu

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.

import cytonaut
import networkx

# example graph:
# a  d--e
# |\ |  |
# | \|  |
# b--c--f


def main():
    G = networkx.Graph()
    G.add_nodes_from('abcdef')
    G.add_edges_from(['ab', 'ac', 'bc', 'de', 'ef', 'cd', 'cf'])
    CG = cytonaut.CytoGraph(G)
    CG.do_nauty(aut=True)
    print("Uncolored graph")
    print("===============")
    print("Canonical string:", CG.canon_string())
    print("Map to canonical form:", CG.canon_translation()    )
    print("Orbits:", CG.orbits())
    print("Automorphisms:")
    for aut in CG.automorphisms():
        print(" ", aut)

    H = G.copy()
    H.nodes['a']['color'] = 1
    H.nodes['b']['color'] = 2
    CH = cytonaut.CytoGraph(H)
    CH.do_nauty(aut=True)
    print
    print("Colored graph")
    print("=============")
    print("Canonical string:", CH.canon_string())
    print("Map to canonical form:", CH.canon_translation()    )
    print("Orbits:", CH.orbits())
    print("Automorphisms:")
    for aut in CH.automorphisms():
        print(" ", aut)

if __name__ == '__main__':
    main()
