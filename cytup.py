from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext


ext = Extension("cytonaut", ["cytonaut.pyx"],
                extra_objects=["./nauty/nauty1.o", "./nauty/gtools.o", "./nauty/naututil1.o", "./nauty/nausparse.o", "./nauty/nautil1.o",
                               "./nauty/naugraph1.o", "./nauty/schreier.o", "./nauty/naurng.o", "./nauty/naugroup.o"],
                language="c++",
                extra_compile_args=["-O3", "-fPIC"],
                include_dirs=["./nauty"])

setup(ext_modules = [ext],
      cmdclass={'build_ext' : build_ext})
